<?php

function precision_generate_css($col_size, $gutter_size, $nr_grids, $margins, $width_type, $max_width, $max_width_type) {
  //Convert the option number into a real css suffix.
  if ($width_type == 0) {
    $width_type = 'px';
  }
  else if ($width_type == 1) {
    $width_type = 'em';
  }
  else if ($width_type == 2) {
    $width_type = '%';
  }

  //If there's no max_width, we don't want a suffix.
  if ($max_width == '') {
    $max_width_type = '';
  }
  else if ($max_width_type == 0) {
    $max_width_type = 'px;';
  }
  else if ($max_width_type == 1) {
    $max_width_type = 'em;';
  }

  $css = '';
  $final_css = array(
    'container' => precision_set_up_container_sizes($nr_grids, $col_size, $gutter_size, $margins, $width_type, $max_width, $max_width_type),
    'grids' => precision_generate_grids($nr_grids, $margins, $width_type, $max_width, $max_width_type),
    'col_sizes' => precision_set_up_col_sizes($nr_grids, $col_size, $gutter_size, $width_type),
    'push' => precision_set_up_push_class($nr_grids, $col_size, $gutter_size, $width_type),
    'pull' => precision_set_up_pull_class($nr_grids, $col_size, $gutter_size, $width_type),
    'prefix' => precision_set_up_prefix_class($nr_grids, $col_size, $gutter_size, $width_type),
    'suffix' => precision_set_up_suffix_class($nr_grids, $col_size, $gutter_size, $width_type),
  );
  foreach ($final_css as $print) {
    $css .= $print;
  }
  return $css;
}


function precision_save_css($name, $css) {
  file_put_contents('public://precision/flexible_' . $name . '.css', $css);
}

/**
 * Generates classes for the number of grids wanted, and gives them some common
 * properties.
 */
function precision_generate_grids($nr_grids, $margins, $width_type) {
  $holder = '';
  for ($i = 1; $i <= $nr_grids; $i++) {
    $holder .= '.grid-' . $i;
    if ($i != $nr_grids) {
      $holder .= ', ';
    }
    else {
      $holder .= ' { display: inline; float: left; position: relative; margin: 0 ' . $margins . 'px; } ';
    }
  }
  // Also add the common classes alpha and omega
  $holder .= '.alpha { margin-left: 0; } .omega { margin-right: 0; }';
  return $holder;
}

/**
 * Sets up the container
 */
function precision_set_up_container_sizes($nr_grids, $col_size, $gutter_size, $margins, $width_type, $max_width, $max_width_type) {
  $holder = '';
  $real_grid = ($col_size * $nr_grids) + ($gutter_size * $nr_grids) - $gutter_size + ($margins * 2);
  $holder .= '.container-' . $nr_grids . ' { margin: 0 auto; max-width: ' . $max_width . $max_width_type . ' } ';
  return $holder;
}

/**
 * Sets up widths for every grid depending on specified widths from grid and
 * gutter sizes.
 */
function precision_set_up_col_sizes($nr_grids, $col_size, $gutter_size, $width_type) {
  $holder = '';
  for ($i = 1; $i <= $nr_grids; $i++) {
    $real_grid = ($col_size * $i) + ($gutter_size * $i) - $gutter_size;
    $holder .= '.grid-' . $i . ' { width: ' . $real_grid . $width_type . '; } ';
  }
  return $holder;
}

/**
 * Sets up the .pull class with values from grid and gutter size
 */
function precision_set_up_pull_class($nr_grids, $col_size, $gutter_size, $width_type) {
  $pull = '';
  for ($i = 1; $i <= $nr_grids; $i++) {
    $width = ($col_size * $i) + ($gutter_size * $i);
    $pull .= '.pull-' . $i . ' { left: -' . $width . $width_type . '; } ';
  }
  return $pull;
}

/**
 * Sets up the .push class with values from grid and gutter size
 */
function precision_set_up_push_class($nr_grids, $col_size, $gutter_size, $width_type) {
  $push = '';
  for ($i = 1; $i <= $nr_grids; $i++) {
    $width = ($col_size * $i) + ($gutter_size * $i);
    $push .= '.push-' . $i . ' { left: ' . $width . $width_type . '; } ';
  }
  return $push;
}

/**
 * Sets up the .prefix class with values from grid and gutter size
 */
function precision_set_up_prefix_class($nr_grids, $col_size, $gutter_size, $width_type) {
  $prefix = '';
  for ($i = 1; $i <= $nr_grids; $i++) {
    $width = ($col_size * $i) + ($gutter_size * $i);
    $prefix .= '.prefix-' . $i . ' { padding-left: ' . $width . $width_type . '; } ';
  }
  return $prefix;
}

/**
 * Sets up the .suffix class with values from grid and gutter size
 */
function precision_set_up_suffix_class($nr_grids, $col_size, $gutter_size, $width_type) {
  $suffix = '';
  for ($i = 1; $i <= $nr_grids; $i++) {
    $width = ($col_size * $i) + ($gutter_size * $i);
    $suffix .= '.suffix-' . $i . ' { padding-right: ' . $width . $width_type .'; } ';
  }
  return $suffix;
}
