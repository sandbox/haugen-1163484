<?php
function precision_generate_image($extension = 'png', $resolution, $settings, $name) {
  
  $dir = 'public://precision/';
  $file = $name . '_overlay';
  $destination =  $dir . $file  . '.' . $extension;
  
  $res = explode('x', $resolution);

  $width = $res[0];
  $height = $res[1];

  // Generate a grid column image for tiling
  $x = $settings['site_template']['gutter_size']/2;
  $im = imagecreate($width, $height);

  $color = imagecolorallocate($im, 255, 255, 255);
  imagefilledrectangle($im, 0, 0, $width, $height, $color);
  $color = imagecolorallocate($im, 255, 200, 200);
  imagefilledrectangle($im, $x, 0, $width - $x, $height, $color);

  $save_function = 'image' . ($extension == 'jpg' ? 'jpeg' : $extension);
  $save_function($im, drupal_realpath($destination));

  $images[$extension][$resolution][$destination] = $destination;

  return $destination;
}
