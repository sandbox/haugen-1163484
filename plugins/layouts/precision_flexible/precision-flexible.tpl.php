<?php

/**
 * Universal template for precision 2.
 * Used with panels everywhere.
 */
?>
<div<?php print $css_id ? " id=\"$css_id\"" : ''; ?> class="page-wrapper">
  <div class="page-body-wrapper">

    <?php if (isset($settings['is_template']) && $settings['is_template'] == 1): ?>
      <div class="region container-<?php print isset($settings['site_template']['nr_cols']) ? $settings['site_template']['nr_cols'] : ''; ?> clearfix">
    <?php endif; ?>

    <?php if (isset($settings['is_template']) && $settings['is_template'] == 0): ?>
      <div class="page-body <?php print isset($settings['site_template']['nr_cols']) ? 'grid-' . $settings['site_template']['nr_cols'] : ''; ?>">
      <div class="page-body-inner clearfix">
    <?php endif; ?>

    <?php if (isset($settings['regions'])) {
      foreach ($settings['regions'] as $name => $region) {
        if (!empty($content[$name])) {
          $omega = $region['omega'] == 1 ? ' omega' : '';
          $alpha = $region['alpha'] == 1 ? ' alpha' : '';
          print '<div class="page-' . str_replace('_', '-', $name) . ' grid-' . $region['grids'] . $omega . $alpha . '">';
          print '<div class="page-' . str_replace('_', '-', $name) . '-inner clearfix">';
          print render($content[$name]);
          print '</div></div>';
        }
      }
    } ?>

    <?php if (isset($settings['is_template']) && $settings['is_template'] == 0): ?>
      </div> <!-- END page-body-inner -->
      </div> <!-- END page-body -->
    <?php endif; ?> 

    <?php if (isset($settings['is_template']) && $settings['is_template'] == 1): ?>
      </div> <!-- END region container-X -->
    <?php endif; ?>
  </div>
</div>
