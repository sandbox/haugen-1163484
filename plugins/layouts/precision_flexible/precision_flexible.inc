<?php

$plugin = array(
  'title' => t('Precision flexible'),
  'theme' => 'precision_flexible',
  'settings form' => 'precision_flexible_settings_form',
  'settings submit' => 'precision_flexible_settings_form_submit',
  'icon' => 'precision-flexible.png',
  'category' => 'Precision',
  'settings' => array(
    'is_template' => 0,
    'col_size' => 40,
    'gutter_size' => 20,
    'nr_cols' => 16,
    'margin' => 10,
    'css_file' => '',
    'regions' => array('main' => t('Main')),
  ),
  'regions function' => 'precision_flexible_regions',
);

function precision_flexible_settings_form($display, $layout, $settings) {
  $form = array();
  $form['#attached']['js'] = array(drupal_get_path('theme', 'precision') . '/plugins/layouts/precision_flexible/precision-flexible.js');
 
  /**
   *  Width type
   */
  $form['width_type'] = array(
    '#type' => 'radios',
    '#title' => 'Width type',
    '#options' => array(t('px'), t('em'), t('%')),
    '#description' => 'Set the width suffix for column/gutter/margin width',
    '#default_value' => isset($settings['width_type']) ? $settings['width_type'] : 0,
  );
  
  /**
   * Site template
   */
  $form['is_template'] = array(
    '#type' => 'radios', 
    '#title' => t('Use as site template'),
    '#default_value' => isset($settings['is_template']) ? $settings['is_template'] : 0,
    '#options' => array(t('No'), t('Yes')),
    '#description' => t('Choose if template is a global site template.'),
  ); 
   
  $form['site_template'] = array(
   '#type' => 'fieldset',
   '#title' => t('Site template settings'),
  );
  
  $form['site_template']['nr_cols'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of columns'),
    '#default_value' => isset($settings['site_template']['nr_cols']) ? $settings['site_template']['nr_cols'] : '',
    '#description' => t('Set the number of columns'),
  );
  $form['site_template']['max-width'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum width'),
    '#default_value' => isset($settings['site_template']['max-width']) ? $settings['site_template']['max-width'] : '',
    '#description' => t('The max width of the page wrapper.'),
  );
  $form['site_template']['max-width-suffix'] = array(
    '#type' => 'radios',
    '#title' => t('Maximum width suffix'),
    '#default_value' => isset($settings['site_template']['max-width-suffix']) ? $settings['site_template']['max-width-suffix'] : '',
    '#options' => array(t('px'), t('em')),
    '#description' => t('The suffix of the max width of the page wrapper.'),
  );

  $form['site_template']['col_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Column width'),
    '#default_value' => isset($settings['site_template']['col_size']) ? $settings['site_template']['col_size'] : '',
    '#description' => t('Set the column width'),
  );

  $form['site_template']['gutter_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Gutter size'),
    '#default_value' => isset($settings['site_template']['gutter_size']) ? $settings['site_template']['gutter_size'] : '',
    '#description' => t('Set the gutter width'),
  );
  
  $form['site_template']['margin'] = array(
    '#type' => 'textfield',
    '#title' => t('Margin'),
    '#default_value' => isset($settings['site_template']['margin']) ? $settings['site_template']['margin'] : '',
    '#description' => t('Set the side margin'),
  );


  if (isset ($form['site_template'])) {
    $form['site_template']['#states'] = array('visible' => array(':input[name="layout_settings[is_template]"]' => array('value' => '1')));
  } 
  else {
    drupal_set_message(t('Field does not exist.'));
  }
 
  /**
   * Regions
   */
  $form['new_region'] = array(
   '#type' => 'fieldset',
   '#title' => t('Add new region'),
  );

  $form['new_region']['region_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Region title')
  );

  $form['new_region']['region_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Region name')
  );

  $form['new_region']['region_grids'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of grids')
  );

  $form['new_region']['omega'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add omega class to region')
  );
  
  $form['new_region']['alpha'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add alpha class to region')
  ); 

  /*
   * Still working on this.
   *
   * layout_designer is a button that should toggle between the normal view
   * and the design view, where you can handle your regions
   *
   * the rest is every region styled as a table. Should add edit and delete
   * button and stuff here later on.
   *
   */

  /*
  $form['layout_designer'] = array(
    '#type' => 'submit',
    '#value' => t('Show layout designer'),
  );
  
  $header = array(
    'Region',
  );

  $rows = array();
  foreach ($settings['regions'] as $region) {
    $rows[] = array($region['title']);
  }

  $variables = array(
    'header' => $header,
    'rows' => $rows,
  );

  $form['regions'] = array(
    '#type' => 'markup',
    '#markup' => theme('table', $variables),
  );
  */
  return $form;
}

function precision_flexible_region_exists($value, $element, $form_state) {
  return FALSE;
}

function precision_flexible_settings_form_submit(&$settings, $display, $layout, $layout_settings) {
  require_once drupal_get_path('theme', 'precision') . '/template.php';
  precision_load_include('precision.generate');
  $name = str_replace(array(':', '_'), array('-', '-'), $display->cache_key);
  if (!is_dir('public://precision')) {
    drupal_mkdir('public://precision', NULL, TRUE);
  }

  // Generate css only if it's a site template
  if($settings['is_template'] == 1) {
    $css = precision_generate_css(
      $settings['site_template']['col_size'], 
      $settings['site_template']['gutter_size'], 
      $settings['site_template']['nr_cols'], 
      $settings['site_template']['margin'],
      $settings['width_type'],
      $settings['site_template']['max-width'],
      $settings['site_template']['max-width-suffix']
    );
      
    precision_save_css($name, $css);
    
    $settings['css_file'] = $name . '.css';
  }
  
  if (isset($layout_settings['regions'])) {
    $settings['regions'] = $layout_settings['regions'];
  }
  else {
    $settings['regions'] = array();
  }
  
  if (!empty($settings['new_region']['region_name']) && !empty($settings['new_region']['region_title']) && !empty($settings['new_region']['region_grids'])) {
    $settings['regions'][$settings['new_region']['region_name']] = array(
      'grids' => $settings['new_region']['region_grids'],
      'title' => $settings['new_region']['region_title'],
      'omega' => $settings['new_region']['omega'],
      'alpha' => $settings['new_region']['alpha']
    );
  }
  unset($settings['new_region']);
}

/**
 * Implementation of theme_preprocess_precision_naked().
 */
function precision_preprocess_precision_flexible(&$vars) {
  precision_check_layout_variables($vars);
  if (!empty($vars['settings']['css_file'])) {
    $path = variable_get('file_public_path', conf_path() . '/files');
    drupal_add_css($path . '/precision/flexible_' . $vars['settings']['css_file']);
  }
}

function precision_flexible_regions($display, $settings, $layout) {
  $regions = array();
  if (isset($settings['regions'])) {
    foreach ($settings['regions'] as $name => $region) {
      $regions[$name] = $region['title'];
    }
  }

  return $regions;
}
